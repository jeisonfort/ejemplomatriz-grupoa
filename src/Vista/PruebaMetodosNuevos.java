/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Vendedor;
import Negocio.SistemaVentas;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Jeison Ferrer Ortega 1152004 y Jairo Guillermo Gil Ureña 1151991
 */
public class PruebaMetodosNuevos {
    
    public static void main(String[] args) throws IOException {
        
        SistemaVentas mySistema=new SistemaVentas("src/Datos/vendedores.xls");
        
        
        // Usando Scanner para obtener información del usuario
        Scanner in = new Scanner(System.in);
        
        System.out.println("Seleccione el metodo que desea probar: ");
        
        System.out.println("1. Obtener los vendedores con ganacias mayores al promedio");
        System.out.println("2. Obtener la menor venta");
        System.out.println("3. Obtener el vendedor con menores ventas acumuladas");
        
        int opcion =in.nextInt();
        
        switch(opcion){
            case 1:
                
                 for(Vendedor vendedor : mySistema.getVendedores_MasVentas())
                System.out.println(vendedor.toString());
                System.out.println("El total encontrado fue de: "
                        +mySistema.getVendedores_MasVentas().length );
                break;
                            
            case 2:
                System.out.println(mySistema.getVenta_Menor());
                break;
            
            case 3 : 
                 System.out.println(mySistema.getMenosVentas().toString());
                break;
        }     
    }
}
